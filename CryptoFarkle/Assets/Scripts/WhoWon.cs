﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;


public class WhoWon : MonoBehaviour
{
    //config params
    bool didPlayerOneWin;
    bool didPlayerTwoWin;
    GameMechanics gameMechs;
    TextMeshProUGUI winText;


    private void Awake()
    {
        int whoWonCount = FindObjectsOfType<WhoWon>().Length;
        if (whoWonCount > 1 )
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }

        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        gameMechs = FindObjectOfType<GameMechanics>();
    }

    // Update is called once per frame
    void Update()
    {
        if(SceneManager.GetActiveScene().name != "WinScreen")
        {
            SetWinBools();
        }
        if(SceneManager.GetActiveScene().name == "WinScreen")
        {
            FindWinText();
            SetWinTextText();
        }
        DestroyMe();
        
    }

    private void SetWinBools()
    {
        didPlayerOneWin = gameMechs.didPlayerOneWin;
        didPlayerTwoWin = gameMechs.didPlayerTwoWin;

    }

    private void FindWinText()
    {
       TextMeshProUGUI[] texts  =  FindObjectsOfType<TextMeshProUGUI>();
        foreach(TextMeshProUGUI possibleTexts in texts)
        {
            if(possibleTexts.name == "WinText")
            {
                winText = possibleTexts;
            }
        }
    }

    private void SetWinTextText()
    {
        if(didPlayerOneWin)
        {
            winText.text = "Player One Wins!!!";
        }
        if (didPlayerTwoWin)
        {
            winText.text = "Player Two Wins!!!";
        }
    }

    private void DestroyMe()
    {
        if(SceneManager.GetActiveScene().name == "MainMenu")
        {
            Destroy(gameObject);
        }
    }
}
