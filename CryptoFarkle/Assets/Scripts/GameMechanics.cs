﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class GameMechanics : MonoBehaviour
{
    // config params
    [SerializeField] Button rollButton;
    [SerializeField] Text scoreText;
    [SerializeField] int roundScore = 0;
    [SerializeField] int gameScore = 0;
    [SerializeField] int winningScore = 3000;

    [SerializeField] Text playerOneScoreText;
    [SerializeField] public int playerOneScore = 0;
    [SerializeField] Text playerTwoScoreText;
    [SerializeField] public int playerTwoScore = 0;

    [SerializeField] Dice[] allDicesRolled;
    int oneDicePoints = 100;
    int fiveDicePoints = 50;

    public bool isPlayerOnesTurn = false;
    public bool isPlayerTwosTurn = false;

    [SerializeField] public Text bustedText;

    // state params

    public List<int> onesRolled = new List<int> { };
    public List<int> twosRolled = new List<int> { };
    public List<int> threesRolled = new List<int> { };
    public List<int> foursRolled = new List<int> { };
    public List<int> fivesRolled = new List<int> { };
    public List<int> sixesRolled = new List<int> { };

    public List<int> onesSelected = new List<int> { };
    public List<int> twosSelected = new List<int> { };
    public List<int> threesSelected = new List<int> { };
    public List<int> foursSelected = new List<int> { };
    public List<int> fivesSelected = new List<int> { };
    public List<int> sixesSelected = new List<int> { };

    public List<Dice> diceSelected = new List<Dice> { };
    Dice[] leftoverDice;

    public bool didPlayerOneWin = false;
    public bool didPlayerTwoWin = false;

    private void Awake()
    {
        int gameMechCount = FindObjectsOfType<GameMechanics>().Length;
        if (gameMechCount > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
     
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        //isPlayerOnesTurn = true;
        bustedText.enabled = false;
        RoundScoreTextGenerator();
        PlayerScoreTextGenerator();
        CollectConfigs();
    }


    private void Update()
    {
        int numberOfDiceLeft = FindObjectsOfType<Dice>().Length;
        if (numberOfDiceLeft == 0)
        {
            PassAndScore();
        }
        CollectConfigs();
        RoundScoreTextGenerator();
        PlayerScoreTextGenerator();
        WinCondition();
        if(SceneManager.GetActiveScene().name == "WinScreen")
        {
            Destroy(gameObject);
        }
    }

    public void ScoreAndRoll()
    {
        ClearSelectedLists();
        UpdateRoundScore();
        RemoveDice();
        ReRollDice();
    }

    public void GetAllDiceNumbersRolled()
    {
        ClearRolledLists();
        Dice[] allDicesRolled = FindObjectsOfType<Dice>();
        foreach (Dice die in allDicesRolled)
        {
            if (die.diceNumberRolled == 1)
            {
                onesRolled.Add(die.diceNumberRolled);
            }
            if (die.diceNumberRolled == 2)
            {
                twosRolled.Add(die.diceNumberRolled);
            }
            if (die.diceNumberRolled == 3)
            {
                threesRolled.Add(die.diceNumberRolled);
            }
            if (die.diceNumberRolled == 4)
            {
                foursRolled.Add(die.diceNumberRolled);
            }
            if (die.diceNumberRolled == 5)
            {
                fivesRolled.Add(die.diceNumberRolled);
            }
            if (die.diceNumberRolled == 6)
            {
                sixesRolled.Add(die.diceNumberRolled);
            }
        }
    }

    public void RollDice()
    {
        Dice[] allDice = FindObjectsOfType<Dice>();
        foreach (Dice die in allDice)
        {
            die.RollDice();
        }
    }

    private void ReRollDice()
    {
        leftoverDice = FindObjectsOfType<Dice>();
        foreach (Dice die in leftoverDice)
        {
            die.RollDice();
        }
    }

    private void RemoveDice()
    {
        foreach (Dice die in diceSelected)
        {
            if (die.isSelected)
            {
                die.RemoveDie();
            }
        }
    }

    public void UpdateRoundScore()
    {
        GetAllSelectedDice();
        //Rolling Ones Score Conditions
        if (onesSelected.Count <= 2)
        {
            roundScore += oneDicePoints * onesSelected.Count;
        }
        else if (onesSelected.Count == 3)
        {
            roundScore += oneDicePoints * 10;
        }
        else if (onesSelected.Count == 4)
        {
            roundScore += oneDicePoints * 20;
        }
        else if (onesSelected.Count == 5)
        {
            roundScore += oneDicePoints * 30;
        }
        else if (onesSelected.Count == 6)
        {
            roundScore += oneDicePoints * 40;
        }
        //Rolling Fives Score Conditions
        if (fivesSelected.Count <= 2)
        {
            roundScore += fiveDicePoints * fivesSelected.Count;
        }
        else if (fivesSelected.Count == 3)
        {
            roundScore += fiveDicePoints * 10;
        }
        else if (fivesSelected.Count == 4)
        {
            roundScore += fiveDicePoints * 20;
        }
        else if (fivesSelected.Count == 5)
        {
            roundScore += fiveDicePoints * 30;
        }
        else if (fivesSelected.Count == 6)
        {
            roundScore += fiveDicePoints * 40;
        }
        //Rolling Twos Score Conditions
        if (twosSelected.Count == 3)
        {
            roundScore += 2 * 100;
        }
        else if (twosSelected.Count == 4)
        {
            roundScore += 2 * 200;
        }
        else if (twosSelected.Count == 5)
        {
            roundScore += 2 * 300;
        }
        else if (twosSelected.Count == 6)
        {
            roundScore += 2 * 400;
        }
        //Rolling Three Score Conditions
        if (threesSelected.Count == 3)
        {
            roundScore += 3 * 100;
        }
        else if (threesSelected.Count == 4)
        {
            roundScore += 3 * 200;
        }
        else if (threesSelected.Count == 5)
        {
            roundScore += 3 * 300;
        }
        else if (threesSelected.Count == 6)
        {
            roundScore += 3 * 400;
        }
        //Rolling Four Score Conditions
        if (foursSelected.Count == 3)
        {
            roundScore += 4 * 100;
        }
        else if (foursSelected.Count == 4)
        {
            roundScore += 4 * 200;
        }
        else if (foursSelected.Count == 5)
        {
            roundScore += 4 * 300;
        }
        else if (foursSelected.Count == 6)
        {
            roundScore += 4 * 400;
        }
        //Rolling Sixes Score Conditions
        if (sixesSelected.Count == 3)
        {
            roundScore += 6 * 100;
        }
        else if (sixesSelected.Count == 4)
        {
            roundScore += 6 * 200;
        }
        else if (sixesSelected.Count == 5)
        {
            roundScore += 6 * 300;
        }
        else if (sixesSelected.Count == 6)
        {
            roundScore += 6 * 400;
        }

        scoreText.text = "Game Score: " + gameScore + "\n" + "Round Score: " + roundScore;
    }

    public void UpdateGameScore()
    {
        if (isPlayerOnesTurn)
        {
            playerOneScore += roundScore;

            playerOneScoreText.text = "Player 1 \n Score: " + playerOneScore;

        }
        if (isPlayerTwosTurn)
        {
            playerTwoScore += roundScore;

            playerTwoScoreText.text = "Player 2 \n Score: " + playerTwoScore;
        }
    }

    public void GetAllSelectedDice()
    {
        ClearSelectedLists();
        Dice[] allDicesRolled = FindObjectsOfType<Dice>();
        foreach (Dice die in allDicesRolled)
        {
            if (die.isSelected == true)
            {
                if (die.diceNumberRolled == 1)
                {
                    onesSelected.Add(die.diceNumberRolled);
                    diceSelected.Add(die);
                }
                if (die.diceNumberRolled == 2)
                {
                    twosSelected.Add(die.diceNumberRolled);
                    diceSelected.Add(die);
                }
                if (die.diceNumberRolled == 3)
                {
                    threesSelected.Add(die.diceNumberRolled);
                    diceSelected.Add(die);
                }
                if (die.diceNumberRolled == 4)
                {
                    foursSelected.Add(die.diceNumberRolled);
                    diceSelected.Add(die);
                }
                if (die.diceNumberRolled == 5)
                {
                    fivesSelected.Add(die.diceNumberRolled);
                    diceSelected.Add(die);
                }
                if (die.diceNumberRolled == 6)
                {
                    sixesSelected.Add(die.diceNumberRolled);
                    diceSelected.Add(die);
                }
            }

        }

    }

    private void ClearRolledLists()
    {
        onesRolled.Clear();
        twosRolled.Clear();
        threesRolled.Clear();
        foursRolled.Clear();
        fivesRolled.Clear();
        sixesRolled.Clear();
    }

    private void ClearSelectedLists()
    {
        onesSelected.Clear();
        twosSelected.Clear();
        threesSelected.Clear();
        foursSelected.Clear();
        fivesSelected.Clear();
        sixesSelected.Clear();
        diceSelected.Clear();
    }

    public void OnBust()
    {
        ClearRolledLists();
        int numberOfDiceLeft = FindObjectsOfType<Dice>().Length;
        GetAllDiceNumbersRolled();
        if (!rollButton.interactable)
        {
            if (onesRolled.Count < 1 & fivesRolled.Count < 1)
            {
                if (twosRolled.Count < 3 & threesRolled.Count < 3 & fivesRolled.Count < 3 & sixesRolled.Count < 3 & numberOfDiceLeft != 0)
                {
                    bustedText.enabled = true;
                    Invoke("ResetScene", 1.0f);
                }
            }
        }

    }

    public void PassAndScore()
    {
        UpdateRoundScore();
        UpdateGameScore();
        WinCondition();
        if(!didPlayerTwoWin & !didPlayerOneWin)
        {
            ResetScene();
        }
    }

    private void ResetScene()
    {
        SceneManager.LoadScene("CoreGame");
        roundScore = 0;
    }

    private void RoundScoreTextGenerator()
    {
        if (isPlayerOnesTurn)
        {
            scoreText.text = "Player One \n Round Score: " + roundScore;
        }
        if (isPlayerTwosTurn)
        {
            scoreText.text = "Player Two \n Round Score: " + roundScore;
        }

    }

    private void PlayerScoreTextGenerator()
    {
        playerOneScoreText.text = "Player 1 Score:\n" + playerOneScore + "/" + winningScore;
        playerTwoScoreText.text = "Player 2 Score:\n" + playerTwoScore + "/" + winningScore;
    }

    private void CollectConfigs()
    {
        Button[] buttonArray = FindObjectsOfType<Button>();
        foreach (Button button in buttonArray)
        {
            if (button.tag == "RollButton")
            {
                rollButton = button;
            }
            if (button.tag == "ScoreAndRoll")
            {
                rollButton = button;
            }
        }
        Text[] textArray = FindObjectsOfType<Text>();
        foreach (Text text in textArray)
        {
            if (text.tag == "Player1Score")
            {
                playerOneScoreText = text;
            }
            if (text.tag == "Player2Score")
            {
                playerTwoScoreText = text;
            }
            if (text.tag == "BustedAlert")
            {
                bustedText = text;
            }
            if (text.tag == "RoundScore")
            {
                scoreText = text;
            }

        }
    }

    public void BustDelay()
    {
        Invoke("OnBust", 0.1f);
    }

    private void WinCondition()
    {
        if (playerOneScore >= winningScore)
        {
            didPlayerOneWin = true;
            SceneManager.LoadScene("WinScreen");
        }
        if (playerTwoScore >= winningScore)
        {
            didPlayerTwoWin = true;
            SceneManager.LoadScene("WinScreen");
        }
    }
}
