﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dice : MonoBehaviour
{
    //config params
    [SerializeField] Sprite[] possibleDiceSprites;
    [SerializeField] Button rollButton;
    public GameMechanics gameMechanics;

    public int diceNumberRolled;

    int possibleDiceSpriteIndex;

    // state params 
    Color defaultColor = new Color(1f, 1f, 1f);

    Color highlightedColor = new Color(0.97f, 0.65f, 0f);

    public bool isSelected = false;

    private void Start()
    {
        HideDice();
    }

    private void OnMouseDown()
    {       
        SelectDice();
    }

    private void SelectDice()
        {
            GameMechanics gameMechanic = FindObjectOfType<GameMechanics>();
            if (!isSelected)
                {
                    if(diceNumberRolled == 1)
                    {
                        isSelected = true;
                        GetComponent<SpriteRenderer>().color = highlightedColor;
                    }

                    if (diceNumberRolled == 5)
                    {
                        isSelected = true;
                        GetComponent<SpriteRenderer>().color = highlightedColor;
                    }

                    if(gameMechanic.twosRolled.Count >= 3 & diceNumberRolled == 2)
                    {
                    isSelected = true;
                    GetComponent<SpriteRenderer>().color = highlightedColor;
                    }

                    if (gameMechanic.threesRolled.Count >= 3 & diceNumberRolled == 3)
                    {
                    isSelected = true;
                    GetComponent<SpriteRenderer>().color = highlightedColor;
                    }

                    if (gameMechanic.foursRolled.Count >= 3 & diceNumberRolled == 4)
                    {
                    isSelected = true;
                    GetComponent<SpriteRenderer>().color = highlightedColor;
                    }

                    if (gameMechanic.sixesRolled.Count >= 3 & diceNumberRolled == 6)
                    {
                        isSelected = true;
                        GetComponent<SpriteRenderer>().color = highlightedColor;
                    }

            }
            else
            {
                isSelected = false;
                GetComponent<SpriteRenderer>().color = defaultColor;
            }
        }

    public void RollDice()
    {
        ShowDice();
        UnSelect();
        int possibleDiceSpriteIndex = Random.Range(0, 6);
        GetComponent<SpriteRenderer>().sprite = possibleDiceSprites[possibleDiceSpriteIndex];
        GenerateDiceNumberRolled(possibleDiceSpriteIndex);
    }

    public int GenerateDiceNumberRolled(int spriteIndex)
    {
        if(spriteIndex == 0)
        {
            diceNumberRolled = 1;
        }
        else if (spriteIndex == 1)
        {
            diceNumberRolled = 2;
        }
        else if (spriteIndex == 2)
        {
            diceNumberRolled = 3;
        }
        else if (spriteIndex == 3)
        {
            diceNumberRolled = 4;
        }
        else if (spriteIndex == 4)
        {
            diceNumberRolled = 5;
        }
        else if (spriteIndex == 5)
        {
            diceNumberRolled = 6;
        }
        return diceNumberRolled;
    }

    private void UnSelect()
    {
        isSelected = false;
        GetComponent<SpriteRenderer>().color = defaultColor;
    }

    private void HideDice()
    {
        GetComponent<SpriteRenderer>().enabled = false;
    }

    private void ShowDice()
    {
        GetComponent<SpriteRenderer>().enabled = true;
    }

    public void RemoveDie()
    {
        if(isSelected)
        {
            Destroy(gameObject);
        }
    }

}
