﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTurnLoader : MonoBehaviour
{
   [SerializeField] GameMechanics gameMechanics;
    [SerializeField] bool isPlyOne;
    [SerializeField] bool isPlyTwo;

    // Start is called before the first frame update
    void Start()
    {
        gameMechanics = FindObjectOfType<GameMechanics>();
        SwitchPlayers();
    }

private void SwitchPlayers()
    {
        if(gameMechanics.isPlayerOnesTurn)
        {
            gameMechanics.isPlayerOnesTurn = false;
            gameMechanics.isPlayerTwosTurn = true;
            gameMechanics.bustedText.enabled = false;
        }
        else if (gameMechanics.isPlayerTwosTurn)
        {
            gameMechanics.isPlayerOnesTurn = true;
            gameMechanics.isPlayerTwosTurn = false;
            gameMechanics.bustedText.enabled = false;
        }
        else
        {
            gameMechanics.isPlayerOnesTurn = true;
        }
    }
}
