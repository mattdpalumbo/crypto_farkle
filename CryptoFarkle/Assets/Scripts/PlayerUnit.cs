﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// A PlayerUnit is a unit controlled by a player


public class PlayerUnit : NetworkBehaviour

{

    private void Awake()
    {
        if (gameObject == null)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }

        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //this function runs an All Player Units, not just ones I own.

        //how do I verify that I am allowed to mess around with this object?

       if (hasAuthority == false)
        {
            return;
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            this.transform.Translate(0, 1, 0);
        }
        
    }
}
