﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerConnectionObject : NetworkBehaviour
{
    
    bool isCoreGameScene = false;
    GameObject myPlayerUnit; 

    private void Awake()
    {
        if (gameObject == null)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }

        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
        //is this my own player?
        if (hasAuthority == false)
        {
            // this object belongs to another player
            return;
        }
        else
        {
            CmdSpawnMyUnit();
        }
        

        // instantiate() only creates object on the local computer.
        // even if it has anetwork intedenty is still will not wxist on 
        // the network and there fore not on any other client unless
        //networkserver.spawn() is called on this object.

        //tell the server to spawn our unit
    }

    public GameObject PlayerUnitPrefab;

    //SyncVars are variables where if their value changes on the Server, then all clients
    // are automatically informed of the new value


    

    [SyncVar(hook = "OnPlayerNameChanged")]
    public string playerName = "Anonymous";

    // Update is called once per frame
    void Update()
    {

        if (isLocalPlayer == false)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            string n = "Quill" + Random.Range(1, 100);

            Debug.Log("Sending the Server a request to change our name to: " + n);
            CmdChangePlayerName(n);

        }
       
    }

    void OnPlayerNameChanged(string newName)
    {
        Debug.Log("OnPlayerNameCHanged -  Old Name " + playerName + " newName: " + newName);

        // WARNING: If you use hook on a SyncVar, then our local value does NOT get automatically
        //updated.
        playerName = newName;
        gameObject.name = "PlayerConnectionObject [" + newName + "]";
    }

    /////////////////////////////////COMMANDS BELOW
    // commands are special functions only executed on server

    
    [Command] 
    void CmdSpawnMyUnit()
    {
        // we are guaranteed to be on the server right now.
        GameObject myPlayerUnit = Instantiate(PlayerUnitPrefab);
        // now that the object exists on the server, propagate it to all the
        // clients and wire up the networkidentity

        NetworkServer.SpawnWithClientAuthority(myPlayerUnit, connectionToClient);
    }

    [Command]
    void CmdChangePlayerName(string n)
    {
        Debug.Log("Cmd Change Player Name " + n);
        playerName = n;


        //maybe we should check that the name doesn't have any blacklisted workds
        // if there is a bad name, do we just ignore this request?
        // or do we still call the rpc but with the original name?
    }

    //RPC
    //RPCs are special functions that ONLY get executed on the clients. 
    
    // tell all the client what this player's name now is.
  

}
