﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonActivator : MonoBehaviour
{
    private void Start()
    {
        EnableInitialRoll();
        DisablePassAndScoreRoll();
    }

    public void EnablePassAndScoreRoll()
    {
        Button[] allButtons = FindObjectsOfType<Button>();
        foreach (Button button in allButtons)
        {
            if (button.name == "Score and Roll Button")
            {
                button.interactable = true;
                button.GetComponentInChildren<Text>().enabled = true;
            }
            if (button.name == "Pass Button")
            {
                button.interactable = true;
                button.GetComponentInChildren<Text>().enabled = true;
            }
        }
    }

    public void EnableInitialRoll()
    {
        Button[] allButtons = FindObjectsOfType<Button>();
        foreach (Button button in allButtons)
        {
            if (button.name == "Initial Roll Button")
            {
                button.interactable = true;
                button.GetComponentInChildren<Text>().enabled = true;
            }
        }
    }

    public void DisablePassAndScoreRoll()
    {
        Button[] allButtons = FindObjectsOfType<Button>();
        foreach (Button button in allButtons)
        {
            if (button.name == "Score and Roll Button" | button.name == "Pass Button")
            {
                button.interactable = false;
                button.GetComponentInChildren<Text>().enabled = false;
            }
        }
    }
    public void DisableInitialRollButton()
    {
        Button[] allButtons = FindObjectsOfType<Button>();
        foreach (Button button in allButtons)
        {
            if (button.name == "Initial Roll Button")
            {
                button.interactable = false;
                button.GetComponentInChildren<Text>().enabled = false;
            }
        }
    }
}
